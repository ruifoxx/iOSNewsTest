import org.junit.*;
import org.junit.runners.MethodSorters;
import page.SingleNewsPage;
import page.StartPage;
import util.Settings;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static util.Helpers.driver;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainTest extends Settings {
    private static StartPage startPage;
    private static SingleNewsPage singleNewsPage;

    @BeforeClass
    public static void setUp() throws Exception {
        uploadApp();
        startPage = new StartPage(driver);
        singleNewsPage = new SingleNewsPage(driver);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void testClickOnFifthNewsAndGetTitle() throws Exception {
        startPage.clickToFifthNewsOnMainPage();
        System.out.println(singleNewsPage.getContextTitle());
        assertThat(singleNewsPage.getContextTitle(), is("Новокузнечанин ограбил старика при свидетелях и попытался скрыться на трамвае"));
    }
}