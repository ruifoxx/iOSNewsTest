package page;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import static util.Settings.namePackage;

public class StartPage {
    private WebDriver driver;

    /**
     * Locators
     */
    private By fifthNews = By.xpath("/XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]");

    /**
     * Methods
     */
    public StartPage(IOSDriver driver) {
        this.driver = driver;
    }

    private void clickNews() {
        driver.findElement(fifthNews).click();
    }

    public void clickToFifthNewsOnMainPage() {
        this.clickNews();
    }
}
