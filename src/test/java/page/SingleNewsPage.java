package page;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SingleNewsPage {
    private WebDriver driver;

    /**
     * Locators
     */
    private By contextTitle = By.xpath("/XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText");

    /**
     * Methods
     */
    public SingleNewsPage(IOSDriver driver) {
        this.driver = driver;
    }

    public String getContextTitle() {
        return driver.findElement(contextTitle).getText();
    }
}
